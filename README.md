# Example java 1.8 tests-project with features from my Java cook-book.

1. KISS
2. Selenide
3. Allure integration
4. AAA-Logger
5. Start/Stop-Logger
6. WebDriverManager
7. Browser choose by property for chrome/firefox
8. HTML/PNG Screenshot on fail with ClassName, methodName and timestamp
9. Property-reading from POM.xml for webdriver and server baseUrl